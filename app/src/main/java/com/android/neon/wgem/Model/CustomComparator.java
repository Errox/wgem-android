package com.android.neon.wgem.Model;

import java.util.Comparator;

public class CustomComparator implements Comparator<ActivityModel> {// may be it would be Model
    @Override
    public int compare(ActivityModel obj1, ActivityModel obj2) {
        return obj1.getStartDate().compareTo(obj2.getStartDate());// compare two objects
    }
}
