package com.android.neon.wgem;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;

import com.android.neon.wgem.Model.ActivityModel;
import com.android.neon.wgem.Model.User;
import com.android.neon.wgem.Service.ActivityRequest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static java.lang.String.valueOf;

public class EditActivityActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        AdapterView.OnItemClickListener, ActivityRequest.ActivityListener {

    private BaseAdapter userAdapter;
    private ArrayList<User> participants = new ArrayList<>();
    private int activity_id;
    private EditText activity_name;
    private EditText activity_description;
    private EditText activity_start_time_time;
    private EditText activity_start_time_date;
    private EditText activity_end_time_date;
    private EditText activity_end_time_time;
    private EditText activity_max_amount_edit;
    private Button button;
    private int adminid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(tokenAvailable()) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_edit_activity);
            int activity_id = getIntent().getIntExtra("activity_id", 0);
            setTitle("Activity Details");

            activity_name   = (EditText)findViewById(R.id.activity_name_edit);
            activity_description = (EditText)findViewById(R.id.activity_description_edit);
            activity_start_time_time   = (EditText)findViewById(R.id.activity_start_time_time_edit);
            activity_start_time_date   = (EditText)findViewById(R.id.activity_start_time_date);
            activity_end_time_time   = (EditText)findViewById(R.id.activity_end_time_time_edit);
            activity_end_time_date   = (EditText)findViewById(R.id.activity_end_time_date_edit);
            activity_max_amount_edit = (EditText)findViewById(R.id.activity_max_amount_edit);


            getInformation(activity_id);
            button = (Button) findViewById(R.id.edit_activity_button);

            final ActivityRequest request = new ActivityRequest(getApplicationContext(), this);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    createActivity(request);
                }
            });
        }else{
            Intent login = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(login);
            finish();
        }

    }

    @Override
    public void onActivitiesAvailable(ArrayList<ActivityModel> activitys) {

    }

    @Override
    public void onParticipantsAvailable(ArrayList<User> users) {

    }



    public void onActivityAvailable(ActivityModel activity) {


        activity_id = activity.getId();
        activity_name.setText(activity.getName());
        activity_description.setText(activity.getDiscription());

        Date start_date = activity.getStartDate();
        Date end_date = activity.getEndDate();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

        activity_start_time_time.setText(timeFormat.format(start_date));
        activity_start_time_date.setText(dateFormat.format(start_date));

        activity_end_time_time.setText(timeFormat.format(end_date));
        activity_end_time_date.setText(dateFormat.format(end_date));
        String participants = valueOf(activity.getMaxParticipants());
        activity_max_amount_edit.setText(participants);

        adminid = activity.getAdmin();
    }


    @Override
    public void onActivityError(String message) {

    }

    @Override
    public void onSignedInActivity() {

    }

    @Override
    public void onActivitySignedinAvailable(boolean b) {

    }

    @Override
    public void onSignedOutActivity() {

    }

    @Override
    public void onActivityUpdate() {
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivityForResult(myIntent, 0);
    }

    public void createActivity(ActivityRequest request){

        activity_name   = (EditText)findViewById(R.id.activity_name_edit);
        activity_description = (EditText)findViewById(R.id.activity_description_edit);
        activity_start_time_time   = (EditText)findViewById(R.id.activity_start_time_time_edit);
        activity_start_time_date   = (EditText)findViewById(R.id.activity_start_time_date);
        activity_end_time_time   = (EditText)findViewById(R.id.activity_end_time_time_edit);
        activity_end_time_date   = (EditText)findViewById(R.id.activity_end_time_date_edit);
        activity_max_amount_edit = (EditText)findViewById(R.id.activity_max_amount_edit);

        String end_time = activity_end_time_date.getText().toString() + " " + activity_end_time_time.getText().toString();
        String start_time = activity_start_time_date.getText().toString() + " " + activity_start_time_time.getText().toString();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        try{
            Date start = dateFormat.parse(start_time);
            Date end = dateFormat.parse(end_time);

            Integer amount = Integer.parseInt(activity_max_amount_edit.getText().toString());
            String name = activity_name.getText().toString();
            String description = activity_description.getText().toString();

            Context context = getApplicationContext();
            SharedPreferences sharedPref = context.getSharedPreferences(
                    getString(R.string.preference_file_key), Context.MODE_PRIVATE);

            int user_id = sharedPref.getInt(getString(R.string.saved_user_id),0);
            ActivityModel activity = new ActivityModel(activity_id,name, description, start, end, amount, user_id);
            postActivity(activity);
        }catch(ParseException e){
            System.out.println("Error: " + e);
        }

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    private boolean tokenAvailable() {
        boolean result = false;
        Context context = getApplicationContext();
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        String token = sharedPref.getString(getString(R.string.saved_token), null);
        if (token != null) {
            result = true;
        }
        return result;
    }

    private void getInformation(int activityId) {
        ActivityRequest request = new ActivityRequest(getApplicationContext(), (ActivityRequest.ActivityListener) this);
        request.handleGetActivitie(activityId);
    }
    private void postActivity(ActivityModel activity){
        ActivityRequest request = new ActivityRequest(getApplicationContext(), this);
        request.handlePutActivity(activity);
    }


}
