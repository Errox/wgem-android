package com.android.neon.wgem.Model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class UserMapper {
    // De JSON attributen die we uitlezen
    public static final String USER_RESULT = "result";
    public static final String USER_NAME = "name";
    public static final String USER_ID = "id";
    public static final String USER_SURNAME = "surname";
    public static final String USER_EMAIL = "email";
    public static final String USER_RESIDENCE = "residence";

    /**
     * Map het JSON response op een arraylist en retourneer deze.
     */
    public static ArrayList<User> mapUserList(JSONObject response){

        ArrayList<User> result = new ArrayList<>();

        try{
            JSONArray jsonArray = response.getJSONArray(USER_RESULT);

            for(int i = 0; i < jsonArray.length(); i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                User user = new User(
                        jsonObject.getInt(USER_ID),
                        jsonObject.getString(USER_NAME),
                        jsonObject.getString(USER_SURNAME),
                        jsonObject.getString(USER_EMAIL),
                        jsonObject.getString(USER_RESIDENCE)
                );
                result.add(user);
            }
        } catch( JSONException ex) {
            Log.e("ActivityMapper", "onPostExecute JSONException " + ex.getLocalizedMessage());
        }
        return result;
    }
}
