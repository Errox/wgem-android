package com.android.neon.wgem.Model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ActivityMapper {
    // De JSON attributen die we uitlezen
    public static final String ACTIVITY_RESULT = "result";
    public static final String ACTIVITY_NAME = "name";
    public static final String ACTIVITY_ID = "id";
    public static final String ACTIVITY_DESCRIPTION = "discription";
    public static final String ACTIVITY_START_DATE = "start_date";
    public static final String ACTIVITY_END_DATE = "end_date";
    public static final String ACTIVITY_PARTICIPANTS = "max_participants";
    public static final String ACTIVITY_ADMIN = "admin_user_id";

    /**
     * Map het JSON response op een arraylist en retourneer deze.
     */
    public static ArrayList<ActivityModel> mapActivityList(JSONObject response){

        ArrayList<ActivityModel> result = new ArrayList<>();

        try{
            JSONArray jsonArray = response.getJSONArray(ACTIVITY_RESULT);

            for(int i = 0; i < jsonArray.length(); i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                // Convert stringdate to Date
                String startdate = jsonObject.getString(ACTIVITY_START_DATE);
                String enddate = jsonObject.getString(ACTIVITY_END_DATE);
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                Date start = dateFormat.parse(startdate);
                Date end = dateFormat.parse(enddate);

                ActivityModel activity = new ActivityModel(
                        jsonObject.getInt(ACTIVITY_ID),
                        jsonObject.getString(ACTIVITY_NAME),
                        jsonObject.getString(ACTIVITY_DESCRIPTION),
                        start,
                        end,
                        jsonObject.getInt(ACTIVITY_PARTICIPANTS),
                        jsonObject.getInt(ACTIVITY_ADMIN)
                );
                result.add(activity);
            }
        } catch( JSONException ex) {
            Log.e("ActivityMapper", "onPostExecute JSONException " + ex.getLocalizedMessage());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }
}
