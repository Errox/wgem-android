package com.android.neon.wgem;

/**
 * Bevat o.a. URL definities.
 */
public class Config {

    private static final String BASIC_URL = "https://wgem.herokuapp.com";

    public static final String URL_LOGIN = BASIC_URL + "/api/login";
    public static final String URL_REGISTER = BASIC_URL + "/api/register";
    public static final String URL_ACTIVITIES = BASIC_URL + "/api/activities";

}
