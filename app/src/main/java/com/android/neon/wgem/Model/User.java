package com.android.neon.wgem.Model;

public class User {
    private int id;
    private String name;
    private String surname;
    private String email;
    private String residence;

    public User(int id, String name, String surname, String email, String residence) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.residence = residence;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getResidence() {
        return residence;
    }

    public void setResidence(String residence) {
        this.residence = residence;
    }


}
