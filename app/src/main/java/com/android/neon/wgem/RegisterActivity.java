package com.android.neon.wgem;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterActivity extends AppCompatActivity {


    public final String TAG = this.getClass().getSimpleName();
    private EditText mEmail;
    private EditText mName;
    private EditText mSurname;
    private EditText mResidence;
    private EditText mPassword;
    private EditText mPassword_repeat;

    View focusView = null;

    public void toaster(String Text){
        Context context = getApplicationContext();
        CharSequence text = Text;
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setTitle("Register");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Button button=(Button)findViewById(R.id.sign_in_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doRegister();
            }
        });


    }

    public boolean onOptionsItemSelected(MenuItem item){
        Intent myIntent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivityForResult(myIntent, 0);
        return true;
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void doRegister(){
        boolean cancel = false;
        mEmail   = (EditText)findViewById(R.id.email);
        mName   = (EditText)findViewById(R.id.name);
        mSurname   = (EditText)findViewById(R.id.surname);
        mResidence   = (EditText)findViewById(R.id.residence);
        mPassword   = (EditText)findViewById(R.id.password);
        mPassword_repeat   = (EditText)findViewById(R.id.password_repeat);

        if (mPassword.getText().toString().equals(mPassword_repeat.getText().toString())) {

            if (!TextUtils.isEmpty(mPassword.getText()) && !isPasswordValid(mPassword.getText().toString())) {
                mPassword.setError(getString(R.string.error_invalid_password));
                focusView = mPassword;
                cancel = true;
            }

            // Check for a valid email address.
            if (TextUtils.isEmpty(mEmail.getText().toString())) {
                mEmail.setError(getString(R.string.error_field_required));
                focusView = mEmail;
                cancel = true;
            } else if (!isEmailValid(mEmail.getText().toString())) {
                mEmail.setError(getString(R.string.error_invalid_email));
                focusView = mEmail;
                cancel = true;
            }

            if (TextUtils.isEmpty(mName.getText().toString())) {
                System.out.println("meuh");
                mName.setError(getString(R.string.error_field_required));
                focusView = mName;
                cancel = true;
            }
            if (TextUtils.isEmpty(mSurname.getText().toString())) {
                mSurname.setError(getString(R.string.error_field_required));
                focusView = mSurname;
                cancel = true;
            }
            if (TextUtils.isEmpty(mResidence.getText().toString())) {
                mResidence.setError(getString(R.string.error_field_required));
                focusView = mResidence;
                cancel = true;
            }

            if (cancel) {
                // There was an error; don't attempt login and focus the first
                // form field with an error.
                focusView.requestFocus();
            } else {
                try {
                    String body = "{\"email\":\"" + mEmail.getText().toString() + "\",\"password\":\"" + mPassword.getText().toString() + "\", \"name\":\"" + mName.getText().toString() + "\", \"surname\":\"" + mSurname.getText().toString() + "\", \"residence\":\"" + mResidence.getText().toString() + "\"}";
                    JSONObject jsonBody = new JSONObject(body);
                    JsonObjectRequest jsObjRequest = new JsonObjectRequest
                            (Request.Method.POST, Config.URL_REGISTER, jsonBody, new Response.Listener<JSONObject>() {

                                @Override
                                public void onResponse(JSONObject response) {
                                    toaster("Succesvol geregistreerd!");
                                    Intent main = new Intent(getApplicationContext(), LoginActivity.class);
                                    startActivity(main);
                                    finish();
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    handleErrorResponse(error);
                                }
                            });

                    jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                            1500, // SOCKET_TIMEOUT_MS,
                            2, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    // Access the RequestQueue through your singleton class.
                    VolleyQueue.getInstance(this.getApplicationContext()).addToRequestQueue(jsObjRequest);
                } catch (JSONException e) {
                    e.printStackTrace();
                    toaster("something went wrong" + e.toString());
                }
            }

        } else {
            toaster("Password isn't the same as your repeated password");
        }

    }

    public void handleErrorResponse(VolleyError error) {
//        Log.e(TAG, "handleErrorResponse");

        if(error instanceof com.android.volley.AuthFailureError) {
            String json = null;
            NetworkResponse response = error.networkResponse;
            if (response != null && response.data != null) {
                json = new String(response.data);
                json = trimMessage(json, "error");
                if (json != null) {
                    json = "Error " + response.statusCode + ": " + json;
//                    toaster(json);
                }
            } else {
                Log.e(TAG, "handleErrorResponse: kon geen networkResponse vinden.");
            }
        } else if(error instanceof com.android.volley.NoConnectionError) {
            Log.e(TAG, "handleErrorResponse: server was niet bereikbaar");
//            txtLoginErrorMsg.setText(getString(R.string.error_server_offline));
        } else {
            Log.e(TAG, "handleErrorResponse: error = " + error);
        }
    }

    public String trimMessage(String json, String key){
        Log.i(TAG, "trimMessage: json = " + json);
        String trimmedString = null;

        try{
            JSONObject obj = new JSONObject(json);
            trimmedString = obj.getString(key);
        } catch(JSONException e){
            e.printStackTrace();
            return null;
        }
        return trimmedString;
    }


    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }


}
