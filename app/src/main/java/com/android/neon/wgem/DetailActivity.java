package com.android.neon.wgem;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.neon.wgem.Model.ActivityModel;
import com.android.neon.wgem.Model.User;
import com.android.neon.wgem.Service.ActivityRequest;
import com.android.neon.wgem.Service.UserAdapter;

import java.util.ArrayList;

public class DetailActivity extends AppCompatActivity implements  ActivityRequest.ActivityListener {
    private ListView listViewParticipants;
    private BaseAdapter userAdapter;
    private ArrayList<User> participants = new ArrayList<>();
    private int activity_id;
    private TextView name;
    private TextView description;
    private TextView start;
    private TextView end;
    private Button button;
    private int adminid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        if(tokenAvailable()){
            super.onCreate(savedInstanceState);

            setContentView(R.layout.activity_detail);
            setTitle("Activity Details");

            name = (TextView) findViewById(R.id.activityName);
            description = (TextView) findViewById(R.id.activityDiscription);
            start = (TextView) findViewById(R.id.activityStartdate);
            end = (TextView) findViewById(R.id.activityEnddate);
            listViewParticipants = (ListView) findViewById(R.id.listViewParticipants);
            userAdapter = new UserAdapter(this, getLayoutInflater(), participants);
            listViewParticipants.setAdapter(userAdapter);

            getInformation((int) getIntent().getLongExtra("ActivityId",0));
            button=(Button)findViewById(R.id.signinButton);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    signinActivity();
                }
            });



        } else {
            //
            // Blijkbaar was er geen token - Naar inlog activity
            //
            Intent login = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(login);
            finish();
        }
    }

    private void signinActivity() {
        ActivityRequest request = new ActivityRequest(getApplicationContext(), (ActivityRequest.ActivityListener) this);
        Context context = getApplicationContext();
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        request.handleSignin((int) getIntent().getLongExtra("ActivityId",0),sharedPref.getInt(getString(R.string.saved_user_id),0));
    }

    private void getInformation(int activityId) {
        ActivityRequest request = new ActivityRequest(getApplicationContext(), (ActivityRequest.ActivityListener) this);
        request.handleGetActivitie(activityId);
        request.handleGetAllParticipants(activityId);
        Context context = getApplicationContext();
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        request.handleGetSigninStatus((int) getIntent().getLongExtra("ActivityId",0),sharedPref.getInt(getString(R.string.saved_user_id),0));
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivityForResult(myIntent, 0);
        return true;
    }

    private boolean tokenAvailable() {
        boolean result = false;
        Context context = getApplicationContext();
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        String token = sharedPref.getString(getString(R.string.saved_token), null);
        if (token != null) {
            result = true;
        }
        return result;
    }

    @Override
    public void onActivitiesAvailable(ArrayList<ActivityModel> activities) {

    }

    @Override
    public void onParticipantsAvailable(ArrayList<User> users) {
        participants.clear();
        for(int i = 0; i < users.size(); i++) {
            participants.add(users.get(i));
        }
        userAdapter.notifyDataSetChanged();
    }

    @Override
    public void onActivityAvailable(ActivityModel activity) {
        activity_id = activity.getId();
       name.setText(activity.getName());
       description.setText(activity.getDiscription());
       start.setText(activity.getStartDate().toLocaleString());
       end.setText(activity.getEndDate().toLocaleString());
       adminid = activity.getAdmin();

        Context context = getApplicationContext();
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        int user_id = sharedPref.getInt(getString(R.string.saved_user_id),0);

        if(adminid == user_id) {
            Button button = (Button) findViewById(R.id.edit);
            button.setVisibility(View.VISIBLE);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent edit = new Intent(getApplicationContext(), EditActivityActivity.class);
                    edit.putExtra("activity_id", activity_id);
                    startActivity(edit);
                }
            });
        }
    }

    @Override
    public void onActivityError(String message) {

    }

    @Override
    public void onSignedInActivity() {
        finish();
        Context context = getApplicationContext();
        CharSequence text = "Singed in";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
        startActivity(getIntent());
    }

    @Override
    public void onActivitySignedinAvailable(boolean b) {
        if(b){
            button.setText("Sign Out");
            button.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    signoutActivity();
                }
            });
        }
    }

    @Override
    public void onSignedOutActivity() {
        finish();
        Context context = getApplicationContext();
        CharSequence text = "Singed out";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
        startActivity(getIntent());
    }

    @Override
    public void onActivityUpdate() {

    }

    private void signoutActivity() {
        ActivityRequest request = new ActivityRequest(getApplicationContext(), (ActivityRequest.ActivityListener) this);
        Context context = getApplicationContext();
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        request.handleSignOut((int) getIntent().getLongExtra("ActivityId",0),sharedPref.getInt(getString(R.string.saved_user_id),0));
    }
}
