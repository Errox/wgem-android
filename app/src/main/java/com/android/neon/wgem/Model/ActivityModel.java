package com.android.neon.wgem.Model;


import java.io.Serializable;
import java.util.Date;

public class ActivityModel implements Serializable {

        private int id;
        private String name;
        private String discription;
        private Date startDate;
        private Date endDate;
        private int admin;
        private int  maxParticipants;

    public ActivityModel(int id,String name, String discription, Date startDate, Date endDate, int maxParticipants, int admin) {
        this.id = id;
        this.name = name;
        this.discription = discription;
        this.startDate = startDate;
        this.endDate = endDate;
        this.maxParticipants = maxParticipants;
        this.admin = admin;
    }


    public ActivityModel(String name, String discription, Date startDate, Date endDate, int maxParticipants, int admin) {
        this.name = name;
        this.discription = discription;
        this.startDate = startDate;
        this.endDate = endDate;
        this.maxParticipants = maxParticipants;
        this.admin = admin;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "ActivityModel{" +
                "name='" + name + '\'' +
                ", discription='" + discription + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", maxParticipants=" + maxParticipants +
                ", admin=" + admin +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getMaxParticipants() {
        return maxParticipants;
    }

    public void setMaxParticipants(int maxParticipants) {
        this.maxParticipants = maxParticipants;
    }

    public int getAdmin() {
        return admin;
    }

    public void setAdmin(int admin) {
        this.admin = admin;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
