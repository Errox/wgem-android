package com.android.neon.wgem;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.neon.wgem.Model.ActivityModel;
import com.android.neon.wgem.Model.CustomComparator;
import com.android.neon.wgem.Model.User;
import com.android.neon.wgem.Service.ActivityAdapter;
import com.android.neon.wgem.Service.ActivityRequest;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        AdapterView.OnItemClickListener, ActivityRequest.ActivityListener {


    public static final int MY_REQUEST_CODE = 1234;
    private ListView listViewActivities;
    private BaseAdapter activityAdapter;
    private ArrayList<ActivityModel> activityModels = new ArrayList<>();

    public void toaster(String Text){
        Context context = getApplicationContext();
        CharSequence text = Text;
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Wie gaat er mee?");

        if(tokenAvailable()){
            setContentView(R.layout.activity_main);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent newActivity = new Intent(getApplicationContext(), CreateActivityActivity.class);
                    startActivityForResult( newActivity, MY_REQUEST_CODE );
                }
            });

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            toggle.syncState();

            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);

            //button on click listener
            listViewActivities = (ListView) findViewById(R.id.listViewActivities);
            listViewActivities.setOnItemClickListener(this);
            activityAdapter = new ActivityAdapter(this, getLayoutInflater(), activityModels);
            listViewActivities.setAdapter(activityAdapter);
            getActivities();

        } else {
            //
            // Blijkbaar was er geen token - Naar inlog activity
            //
            Intent login = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(login);
            finish();
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            toaster("This is a settings button");
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_logout) {
            Context context = getApplicationContext();
            SharedPreferences sharedPref = context.getSharedPreferences(
                    getString(R.string.preference_file_key), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(getString(R.string.saved_token), null);
            editor.putInt(getString(R.string.saved_user_id), 0);
            editor.commit();

            Intent main = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(main);
            finish();
        } else if (id == R.id.action_settings) {

//            Intent main = new Intent(getApplicationContext(), UserActivity.class);
//            startActivity(main);
//            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private boolean tokenAvailable() {
        boolean result = false;
        Context context = getApplicationContext();
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        String token = sharedPref.getString(getString(R.string.saved_token), null);
        if (token != null) {
            result = true;
        }
        return result;
    }

    private void getActivities(){
        ActivityRequest request = new ActivityRequest(getApplicationContext(), this);
        request.handleGetAllActivities();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent newActivity = new Intent(getApplicationContext(), DetailActivity.class);
        newActivity.putExtra("ActivityId", id);
        startActivity( newActivity);
    }

    @Override
    public void onActivitiesAvailable(ArrayList<ActivityModel> activityArrayList) {

        activityModels.clear();
        Collections.sort(activityArrayList, new CustomComparator());
        for(int i = 0; i < activityArrayList.size(); i++) {
            activityModels.add(activityArrayList.get(i));
        }
        activityAdapter.notifyDataSetChanged();
    }

    @Override
    public void onParticipantsAvailable(ArrayList<User> users) {

    }

    @Override
    public void onActivityAvailable(ActivityModel activityModel) {

    }

    @Override
    public void onActivityError(String message) {

    }

    @Override
    public void onSignedInActivity() {

    }

    @Override
    public void onActivitySignedinAvailable(boolean b) {

    }

    @Override
    public void onSignedOutActivity() {

    }

    @Override
    public void onActivityUpdate() {

    }
}
