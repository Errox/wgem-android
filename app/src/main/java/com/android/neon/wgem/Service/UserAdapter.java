package com.android.neon.wgem.Service;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.neon.wgem.Model.User;
import com.android.neon.wgem.R;

import java.util.ArrayList;

public class UserAdapter extends BaseAdapter {

    private final String TAG = this.getClass().getSimpleName();

    private Context mContext;
    private LayoutInflater mInflator;
    private ArrayList<User> userArrayList;

    public UserAdapter(Context context, LayoutInflater layoutInflater, ArrayList<User> userArrayList) {
        this.mContext = context;
        this.mInflator = layoutInflater;
        this.userArrayList = userArrayList;
    }

    @Override
    public int getCount() {
        int size = userArrayList.size();
        return size;
    }

    @Override
    public Object getItem(int position) {
        Log.i(TAG, "getItem() at " + position);
        return userArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return this.userArrayList.get(0).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.i(TAG, "getView at " + position);

        ViewHolder viewHolder;

        if(convertView == null){

            Log.i(TAG, "convertView is NULL - nieuwe maken");

            // Koppel de convertView aan de layout van onze eigen row
            convertView = mInflator.inflate(R.layout.list_activity_row, null);

            // Maak een ViewHolder en koppel de schermvelden aan de velden uit onze eigen row.
            viewHolder = new ViewHolder();
            viewHolder.textViewTitle = (TextView) convertView.findViewById(R.id.rowActivtyName);


            // Sla de viewholder op in de convertView
            convertView.setTag(viewHolder);
        } else {
            Log.i(TAG, "convertView BESTOND AL - hergebruik");
            viewHolder = (ViewHolder) convertView.getTag();
        }

        User user = userArrayList.get(position);
        viewHolder.textViewTitle.setText(user.getName() + " " + user.getSurname() + " " + user.getEmail());

        return convertView;
    }

    private static class ViewHolder {
        public TextView textViewTitle;
        // public TextView textViewContents;
    }
}
