package com.android.neon.wgem;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;

import com.android.neon.wgem.Model.ActivityModel;
import com.android.neon.wgem.Model.User;
import com.android.neon.wgem.Service.ActivityRequest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class CreateActivityActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        AdapterView.OnItemClickListener, ActivityRequest.ActivityListener{

    private EditText activity_name;
    private EditText activity_description;
    private EditText activity_start_time_time;
    private EditText activity_start_time_date;
    private EditText activity_end_time_date;
    private EditText activity_end_time_time;
    private EditText activity_max_amount;
    final Calendar myCalendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        if(tokenAvailable()) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_create_activity);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            setTitle("Create a activity");
            final ActivityRequest request = new ActivityRequest(getApplicationContext(), this);
            Button button = (Button) findViewById(R.id.create_activity_button);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    createActivity(request);
                }
            });
        } else {
            Intent login = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(login);
            finish();
        }


    }

    public void createActivity(ActivityRequest request){

        activity_name   = (EditText)findViewById(R.id.activity_name);
        activity_description = (EditText)findViewById(R.id.activity_description);
        activity_start_time_time   = (EditText)findViewById(R.id.activity_start_time_time);
        activity_start_time_date   = (EditText)findViewById(R.id.activity_start_time_date);
        activity_end_time_time   = (EditText)findViewById(R.id.activity_end_time_time);
        activity_end_time_date   = (EditText)findViewById(R.id.activity_end_time_date);
        activity_max_amount = (EditText)findViewById(R.id.activity_max_amount);
        Date start;
        Date end;

        String end_time = activity_end_time_date.getText().toString() + " " + activity_end_time_time.getText().toString();
        String start_time = activity_start_time_date.getText().toString() + " " + activity_start_time_time.getText().toString();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        try{
            start = dateFormat.parse(start_time);
            end = dateFormat.parse(end_time);

            Integer amount = Integer.parseInt(activity_max_amount.getText().toString());
            String name = activity_name.getText().toString();
            String description = activity_description.getText().toString();

            Context context = getApplicationContext();
            SharedPreferences sharedPref = context.getSharedPreferences(
                    getString(R.string.preference_file_key), Context.MODE_PRIVATE);

            int user_id = sharedPref.getInt(getString(R.string.saved_user_id),0);
            ActivityModel activity = new ActivityModel(name, description, start, end, amount, user_id);
            postActivity(activity);
        }catch(ParseException e){
            System.out.println("Error: " + e);
        }

    }

    private void postActivity(ActivityModel activity){
        ActivityRequest request = new ActivityRequest(getApplicationContext(), this);
        request.handlePostActivity(activity);
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivityForResult(myIntent, 0);
        return true;
    }



    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onActivitiesAvailable(ArrayList<ActivityModel> activities) {

    }

    @Override
    public void onParticipantsAvailable(ArrayList<User> users) {

    }

    @Override
    public void onActivityAvailable(ActivityModel activity) {
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivityForResult(myIntent, 0);
    }

    @Override
    public void onActivityError(String message) {

    }

    @Override
    public void onSignedInActivity() {

    }

    @Override
    public void onActivitySignedinAvailable(boolean b) {

    }

    @Override
    public void onSignedOutActivity() {

    }

    @Override
    public void onActivityUpdate() {

    }

    private boolean tokenAvailable() {
        boolean result = false;
        Context context = getApplicationContext();
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        String token = sharedPref.getString(getString(R.string.saved_token), null);
        if (token != null) {
            result = true;
        }
        return result;
    }

}
